  <div class="box">  
<div class="Block">
        <div class="Block-body">
        
      <?php if ($title): ?>
<div class="BlockHeader">
            <div class="header-tag-icon">
                <div class="BlockHeader-text">
        
          <?php echo $title; ?>

                </div>
            </div>
            <div class="l"></div>
            <div class="r"><div></div></div>
        </div>
        
      <?php endif; ?>

<div class="BlockContent">
          <div class="BlockContent-body">
      
        <?php echo $content; ?>

          </div>
      </div>
          

        </div>
    </div>
        
  </div>
